using Microsoft.VisualStudio.TestTools.UnitTesting;
using BookStore.Services;
using BookStore.Repository;
using BookStore.Entities;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BookUnitTest
{
    [TestClass]
    public class BookUnitTesting
    {
        BookStoreDbContext bookStoreDbContext = new BookStoreDbContext();
        IBookServices bookServices;
        IBookRepository bookRepository;

       [TestInitialize]
       public void InitializeForTest()
        {
            bookRepository = new BookRepository(bookStoreDbContext);
            bookServices = new BookServices(bookRepository);
        }

        [TestMethod]
        public async Task TestAddBooks()
        {
            Books book = new Books();
            book.BookName = "java";
            book.Author = "pooja";
            book.Quantity = 5;
            book.AvailableFor = "Sale";
            book.Cost = 300;
            var result = await bookServices.AddBooks(book);
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public async Task TestGetAllBooks()
        {
            
            List<Books> book = new List<Books>();   //List<Book> books
            var result = await bookServices.GetAllBooks();
            Assert.AreEqual(result.Count, book.Count);
        }

        [TestMethod]

        public async Task TestGetCountOfAvailableBooks()
        {
            var result = await bookServices.GetCountOfAvailableBooks("Rent");
            Assert.AreEqual(result, 3);
        }
    }
}
