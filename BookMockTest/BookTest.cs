using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Services;
using BookStore.Repository;
using Moq;
using System.Collections.Generic;

namespace BookMockTest
{
    [TestClass]
    public class BookTest
    {
        [TestMethod]
        public async Task TestAddBooks()
        {
            var mock = new Mock<IBookRepository>();
            Books book = new Books();
            mock.Setup(p => p.AddBook(book)).ReturnsAsync(true);
            BookServices bookServices = new BookServices(mock.Object);
            bool result = await bookServices.AddBooks(book);
            Assert.AreEqual(result, true);
        }

        [TestMethod]

        public async Task TestGetCountOfAvailableBooks()
        {
            var mock = new Mock<IBookRepository>();
            Books book = new Books();
            mock.Setup(p => p.GetCountOfAvailableBooks("Rent")).ReturnsAsync(2);
            BookServices bookServices = new BookServices(mock.Object);
            int result = await bookServices.GetCountOfAvailableBooks("Rent");
            Assert.AreEqual(result, 2);
        }

        [TestMethod]

        public async Task TestUpdateBook()
        {
            var mock = new Mock<IBookRepository>();
            Books book = new Books();
            book.BookName = "Phyton";
            book.Author = "Pooja";
            book.Quantity = 5;
            book.AvailableFor = "Rent";
            book.Cost = 400;
            mock.Setup(p => p.UpdateBook(book)).ReturnsAsync(true);
            BookServices bookServices = new BookServices(mock.Object);
            bool result = await bookServices.UpdateBook(book);
            Assert.AreEqual(result, true);
        }

        [TestMethod]

        public async Task TestGetAllBooks()
        {
            var mock = new Mock<IBookRepository>();
            List<Books> book = new List<Books>();
            mock.Setup(p => p.GetAllBooks()).ReturnsAsync(book);
            BookServices bookServices = new BookServices(mock.Object);
            var result = await bookServices.GetAllBooks();
            Assert.AreEqual(result, book);
        }

        [TestMethod]

        public async Task TestDeleteBook()
        {
            var mock = new Mock< IBookRepository > ();
            Books book = new Books();
            mock.Setup(p => p.DeleteBook(1)).ReturnsAsync(true);
            BookServices bookServices = new BookServices(mock.Object);
            var result = await bookServices.DeleteBook(1);
            Assert.AreEqual(result, true);
        }

        [TestMethod]

        public async Task TestGetListOfAvailableBooks()
        {
            var mock = new Mock<IBookRepository>();
            List<Books> book = new List<Books>();
            mock.Setup(p => p.GetListOfAvailableBooks("Rent")).ReturnsAsync(book);
            BookServices bookServices = new BookServices(mock.Object);
            var result = await bookServices.GetListOfAvailableBooks("Rent");
            Assert.AreEqual(result, book);
        }
    }
}
