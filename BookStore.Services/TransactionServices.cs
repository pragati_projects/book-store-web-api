﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Repository;

namespace BookStore.Services
{
    public class TransactionServices : ITransactionServices
    {
        private readonly ITransactionRepository _trepository;

        public TransactionServices(ITransactionRepository transactionRepository)
        {
            _trepository = transactionRepository;
        }

        public async  Task<bool> AddTransaction(Transaction transaction)
        {
            return await _trepository.AddTransaction(transaction);
        }

        public async Task<List<Transaction>> GetTransactionDetails()
        {
            return await _trepository.GetTransactionDetails();
        }
    }
}
