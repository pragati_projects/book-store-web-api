﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Services
{
    public interface IUserServices
    {
        Task<bool> AddUser(Users user);
        Task<List<Users>> GetUser();
        Task<bool> DeleteUser(int id);
        Task<bool> UpdateUser(Users user);
    }
}
