﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Repository;

namespace BookStore.Services
{
    public class UserServices : IUserServices
    {
        private readonly IUserRepository _urepository;

        public UserServices(IUserRepository userRepository)
        {
            _urepository = userRepository;
        }

        public async Task<bool> AddUser(Users user)
        {
            return await _urepository.AddUser(user);
        }

        public async Task<bool> DeleteUser(int id)
        {
            return await _urepository.DeleteUser(id);
        }

        public async Task<List<Users>> GetUser()
        {
            return await _urepository.GetUser();
        }

        public async  Task<bool> UpdateUser(Users user)
        {
            return await _urepository.UpdateUser(user);
        }
    }
}
