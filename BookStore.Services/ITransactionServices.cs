﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Services
{
    public interface ITransactionServices
    {
        Task<bool> AddTransaction(Transaction transaction);
        Task<List<Transaction>> GetTransactionDetails();
    }
}
