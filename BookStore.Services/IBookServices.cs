﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Services
{
    public interface IBookServices
    {
        Task<bool> AddBooks(Books book);
        Task<List<Books>> GetUnavailableBooks();
        Task<List<Books>> GetListOfAvailableBooks(string availableFor);
        Task<int> GetCountOfAvailableBooks(string availableFor);
        Task<bool> UpdateBook(Books book);
        Task<bool> DeleteBook(int id);
        Task<List<Books>> GetAllBooks();
        Task<int> GetFee(int userId, int bookId, DateTime rentingBook, DateTime returningBook);
    }
}
