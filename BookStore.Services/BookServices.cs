﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Repository;

namespace BookStore.Services
{
    public class BookServices : IBookServices
    {
        private readonly IBookRepository _brepository;

        public BookServices(IBookRepository bookRepository)
        {
            _brepository = bookRepository;
        }

        public async Task<bool> AddBooks(Books book)
        {
                return await _brepository.AddBook(book);           
        }

        public async Task<bool> DeleteBook(int id)
        {
            return await _brepository.DeleteBook(id);
        }

        public async Task<List<Books>> GetAllBooks()
        {
            return await _brepository.GetAllBooks();
        }

        public async Task<int> GetCountOfAvailableBooks(string availableFor)
        {
              return await _brepository.GetCountOfAvailableBooks(availableFor);    
        }

        public async Task<int> GetFee(int userId, int bookId, DateTime rentingBook, DateTime returningBook)
        {
            return await _brepository.GetFee(userId, bookId, rentingBook, returningBook);
        }

        public async Task<List<Books>> GetListOfAvailableBooks(string availableFor)
        {
            return await _brepository.GetListOfAvailableBooks(availableFor);
        }

        public async Task<List<Books>> GetUnavailableBooks()
        {
            return await _brepository.GetUnavailableBooks();
        }

        public async  Task<bool> UpdateBook(Books book)
        {
            return await _brepository.UpdateBook(book);
        }
    }
}
