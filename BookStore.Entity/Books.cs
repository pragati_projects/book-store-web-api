﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.Entities
{
    public class Books
    {
        [Key]
        [Required]
        public int BookId { get; set; }

        [Required]
        public string BookName { get; set; }

        [Required]
        public string Author { get; set; }

        [Required]
        public int Cost { get; set; }

        [Required]
        public string AvailableFor { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}
