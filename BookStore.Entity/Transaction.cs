﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.Entities
{
    public class Transaction
    {
        [Key]
        [Required]
        public int TransactionId { get; set; }

        [Required]
        [ForeignKey("BookId")]
        public int BookId { get; set; }

        [Required]
        [ForeignKey("UserId")]
        public int UserId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public string AvailedAs { get; set; }
    }
}
