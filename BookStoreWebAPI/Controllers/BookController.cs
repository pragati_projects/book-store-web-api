﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Services;
using BookStoreCustomExceptions;

namespace BookStoreWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookServices _bservices;

        public BookController(IBookServices bookServices)
        {
            _bservices = bookServices;
        }

        [HttpPost]
        public async Task<IActionResult> AddBooks(Books book)
            
        {
            try
            {
                return Ok(await _bservices.AddBooks(book));
            }
            catch(SQlException ex)
            {
                return BadRequest(ex.Message);
            } 
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBooks()
        {
            try
            {
                return Ok(await _bservices.GetAllBooks());
            }
            catch(NoDataFound ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("DeleteBook/{id}")]
        public async Task<IActionResult> DeleteBook(int id)
        {
            try
            {
                return Ok(await _bservices.DeleteBook(id));
            }
            catch(NoDataFound ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("UpdateBook")]
        public async Task<IActionResult> UpdateBook(Books book)
        {
            try
            {
                return Ok(await _bservices.UpdateBook(book));
            }
            catch(NoDataFound ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("CountBooks")]
        public async Task<IActionResult> GetCountOfAvailableBooks(string availableFor)
        {
            try
            {
                return Ok(await _bservices.GetCountOfAvailableBooks(availableFor));
            }
            catch(NoDataFound ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("ListOfBooks")]

        public async Task<IActionResult> GetListOfAvailableBooks(string availableFor)
        {
            try
            {
                return Ok(await _bservices.GetListOfAvailableBooks(availableFor));
            }
            catch(NoDataFound ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("ListOfUnavailableBooks")]

        public async Task<IActionResult> GetUnavailableBooks()
        {
            try
            {
                return Ok(await _bservices.GetUnavailableBooks());
            }
            catch(NoDataFound ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("GetFee")]

        public async Task<IActionResult> GetFee(int userId , int bookId , DateTime rentingBook , DateTime returningBook)
        {
            try
            {
                return Ok(await _bservices.GetFee(userId, bookId, rentingBook, returningBook));
            }
            catch(NoDataFound ex)
            {
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
