﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Services;

namespace BookStoreWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserServices _uservices;

        public UserController(IUserServices userServices)
        {
            _uservices = userServices;
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(Users user)
        {
            return Ok(await _uservices.AddUser(user));
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            return Ok(await _uservices.GetUser());
        }

        [HttpDelete("Delete/{id}")]

        public async Task<IActionResult> DeleteUser(int id)
        {
            return Ok(await _uservices.DeleteUser(id));
        }

        [HttpPost("UpdateUser")]

        public async Task<IActionResult> UpdateUser(Users user)
        {
            return Ok(await _uservices.UpdateUser(user));
        }
    }
}
