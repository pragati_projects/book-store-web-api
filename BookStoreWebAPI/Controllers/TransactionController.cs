﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Services;

namespace BookStoreWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionServices _tservices;

        public TransactionController(ITransactionServices transactionServices)
        {
            _tservices = transactionServices;
        }

        [HttpPost]
        public async Task<IActionResult> AddTransaction(Transaction transaction)
        {
            return Ok(await _tservices.AddTransaction(transaction));
        }

        [HttpGet]
        public async Task<IActionResult> GetTransactionDetails()
        {
            return Ok(await _tservices.GetTransactionDetails());
        }


    }
}
