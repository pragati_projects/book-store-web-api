﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreCustomExceptions
{
    public class SQlException : Exception
    {
        public SQlException()
        {

        }

        public SQlException(string msg , Exception innerException) : base(msg , innerException)
        {

        }

    }
}
