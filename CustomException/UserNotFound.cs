﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStoreCustomExceptions
{
    public class UserNotFound : Exception
    {
        public UserNotFound()
        {

        }

        public UserNotFound(string msg): base(msg)
        {

        }
        public UserNotFound(string msg, Exception innerException): base(msg, innerException)
        {

        }
    }
}
