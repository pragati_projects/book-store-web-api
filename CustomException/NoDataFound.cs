﻿using System;

namespace BookStoreCustomExceptions
{
    public class NoDataFound : Exception
    {
        public NoDataFound()
        {

        }

        public NoDataFound(string msg) : base(msg)
        {

        }
        public NoDataFound(string msg , Exception innerException): base(msg , innerException)
        {

        }
    }
}
