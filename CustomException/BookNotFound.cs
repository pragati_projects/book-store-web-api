﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStoreCustomExceptions
{
    public class BookNotFound : Exception
    {
        public BookNotFound()
        {

        }

        public BookNotFound(string msg) : base(msg)
        {

        }
        public BookNotFound(string msg , Exception innerException): base(msg , innerException)
        {

        }

    }
}
