﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Repository
{
    public interface IUserRepository
    {
        Task<bool> AddUser(Users user);
        Task<bool> DeleteUser(int id);
        Task<List<Users>> GetUser();
        Task<bool> UpdateUser(Users user);
    }
}
