﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStoreCustomExceptions;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Repository
{
    public class BookRepository : IBookRepository
    {
        private readonly BookStoreDbContext _bsDbContext;

        public BookRepository(BookStoreDbContext bookStoreDbContext)
        {
            _bsDbContext = bookStoreDbContext;
        }

        //---------------------------BOOKSTORE MANAGEMENT OPERATIONS-------------------------------------------//
        /*
        * 'AddBook' method return true if the data is inserted else returns false
        * <param name = 'book' ></param>
        */

        public async Task<bool> AddBook(Books book)
        {
          
            try
            {
                Books bookTemp = await _bsDbContext.Book.FirstOrDefaultAsync(b => b.BookName.Equals(book.BookName));
                if (bookTemp == null)
                {
                    int EntryAdded = 0;
                    _bsDbContext.Add(book);
                    EntryAdded = await _bsDbContext.SaveChangesAsync();
                    if (EntryAdded == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    bookTemp.Quantity = bookTemp.Quantity + book.Quantity;
                    int EntryAdded = 0;
                    EntryAdded = await _bsDbContext.SaveChangesAsync();
                    if (EntryAdded == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch(Exception ex)
            {
                throw new SQlException("Sorry could not connect!",ex);
            }
        }

        /*
       * 'DeleteBook' method return true if the data is removed else returns false
       * <param name = 'id' ></param>
       */

        public async Task<bool> DeleteBook(int id)
        {
            try
            {
                Books book = await _bsDbContext.Book.FirstOrDefaultAsync(t => t.BookId == id);
                if (book != null)
                {
                    int EntryAdded = 0;
                    _bsDbContext.Remove(book);
                    EntryAdded = await _bsDbContext.SaveChangesAsync();
                    if (EntryAdded == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    throw new NoDataFound("The data you're searching for is not present");
                }
            }
            catch(Exception ex)
            {
                throw new NoDataFound("No data found",ex);
            }            
        }

      /*
     * 'GetAllBooks' method return true if the data is not null else returns false and throws an exception
     */

        public async Task<List<Books>> GetAllBooks()
        {
             try
            {
            List<Books> book = await _bsDbContext.Book.ToListAsync();
                if (book == null)
                {
                    throw new NoDataFound("The list is empty! There are no books currently in store");
                }
                return book;
            }
            catch(Exception ex)
            {
                throw new NoDataFound("Error",ex);
            }
            
        }


      /*
     * 'GetCountOfAvailableBook' method return true if the data is either "rent" or "sale" and gives the count or else returns false
     * <param name = 'availableFor' ></param>
     */

        public async Task<int> GetCountOfAvailableBooks(string availableFor)
        {
            try
            {
                List<Books> book = await _bsDbContext.Book.Where(b => b.AvailableFor == availableFor).ToListAsync();
                if (book == null)
                {
                    throw new NoDataFound("There are no books present");
                }
                else
                {
                    return book.Count;
                }
            }
            catch(Exception ex)
            {
                throw new NoDataFound("No books present" , ex);
            }
        }


        /*
       * 'GetFee' method return true if the date exceeded by 10 days and need to pay fine by
       * 10rs for each book or else returns false
       * <param name = 'userId , bookId , rentingBook , returningBook' ></param>
       */

        public async Task<int> GetFee(int userId, int bookId, DateTime rentingBook, DateTime returningBook)
        {
            int amount = 0;
            int permissionDaysForRent = 10;
            int count = 1;
            List<Transaction> transactionTemp = await _bsDbContext.Transactions.ToListAsync();
            if (transactionTemp != null)
            {
                foreach(Transaction data in transactionTemp)
                {
                    if(data.UserId==userId && data.BookId==bookId && data.AvailedAs == "Rent")
                    {
                        count = 0;
                        amount = data.Quantity;
                    }
                }
                if (count == 1)
                {
                    throw new BookNotFound("No book is present with this userid");
                }

                string DurationDate = ((returningBook - rentingBook).TotalDays.ToString());
                int DifferenceInTime = Convert.ToInt32(DurationDate);
                if (DifferenceInTime > permissionDaysForRent)
                {
                    return amount * 10;
                }
            }
            return 0;
        }


        /*
       * 'GetListOfAvailableBook' method return true if the data is either "rent" or "sale" and gives the list
       * of all the available books or else returns false if there are no books present and throws exception
       * <param name = 'availableFor' ></param>
       */

        public async Task<List<Books>> GetListOfAvailableBooks(string availableFor)
        {
            try
            {
                List<Books> book = await _bsDbContext.Book.Where(b => b.AvailableFor == availableFor).ToListAsync();
                if (book == null)
                {
                    throw new NoDataFound("Books are not present");
                }
                else
                {
                    return book;
                }
            }
            catch(Exception ex)
            {
                throw new NoDataFound("Please add books",ex);
            }

        }


        /*
       * 'GetListOfUnAvailableBook' method return true if the data is neither "rent" or "sale" 
       * and gives the list of it or else  returns false and gives you 0 value
       */

        public async Task<List<Books>> GetUnavailableBooks()
        {
            try
            {
                List<Books> book = await _bsDbContext.Book.Where(b => b.AvailableFor != "Sale" && b.AvailableFor != "Rent").ToListAsync();
                if (book == null)
                {
                    throw new NoDataFound("No books available");
                }
                else
                {
                    return book;
                }
            }
            catch (Exception ex)
            {
                throw new NoDataFound("Please add books",ex);
            }
        }


        /*
       * 'UpdateBook' method return true if the id entered is same and updates the value
       * or else returns false if there are no books present at all
       * <param name = 'book' ></param>
       */

        public async Task<bool> UpdateBook(Books book)
        {
            try
            {
                Books bookTemp = await _bsDbContext.Book.FirstOrDefaultAsync(u => u.BookId == book.BookId);
                if (bookTemp != null)
                {
                    bookTemp.BookName = book.BookName;
                    bookTemp.Author = book.Author;
                    bookTemp.AvailableFor = book.AvailableFor;
                    bookTemp.Cost = book.Cost;

                    int EntryAdded = 0;
                    EntryAdded = await _bsDbContext.SaveChangesAsync();
                    if (EntryAdded != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    throw new NoDataFound("Sorry! Data is not present");
                }
            }
            catch(NoDataFound ex)
            {
                throw new NoDataFound("Please add data", ex);
            }
            catch(Exception ex)
            {
                throw new SQlException("Sorry!Could not connect",ex);
            }
        }
    }
}
