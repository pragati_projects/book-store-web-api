﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStoreCustomExceptions;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Repository
{
   public  class TransactionRepository : ITransactionRepository
    {
        private readonly BookStoreDbContext _bsDbContext;

        public TransactionRepository(BookStoreDbContext bookStoreDbContext)
        {
            _bsDbContext = bookStoreDbContext;
        }

        public async Task<bool> AddTransaction(Transaction transaction)
        {
            
            try
            {
                Books book = await _bsDbContext.Book.FirstOrDefaultAsync(b => b.BookId == transaction.BookId);
                Users user = await _bsDbContext.User.FirstOrDefaultAsync(u => u.UserId == transaction.UserId);
                if (book != null && user != null)
                {
                    int EntryAdded = 0;
                    if (book.Quantity >= transaction.Quantity)
                    {
                        _bsDbContext.Add(transaction);
                        book.Quantity = book.Quantity - transaction.Quantity;
                        EntryAdded = await _bsDbContext.SaveChangesAsync();
                        if (EntryAdded == 0)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        throw new NoDataFound("Limit Exceeded");
                    }
                }
                else
                {
                    throw new NoDataFound("No data found");
                }
            }
            catch(NoDataFound ex)
            {
                throw new NoDataFound(ex.Message);
            }
            catch(Exception ex)
            {
                throw new SQlException("couldnt connect", ex);
            }
        }

        public async Task<List<Transaction>> GetTransactionDetails()
        {
            try
            {
                List<Transaction> transaction = await _bsDbContext.Transactions.ToListAsync();
                return transaction;
            }
            catch(Exception ex)
            {
                throw new SQlException("couldnt connect", ex);
            }
        }
    }
}
