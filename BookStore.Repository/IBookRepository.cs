﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Repository
{
    public interface IBookRepository
    {
        Task<bool> AddBook(Books book);
        Task<int> GetCountOfAvailableBooks(string availableFor);
        Task<List<Books>> GetAllBooks();
        Task<bool> DeleteBook(int id);
        Task<List<Books>> GetListOfAvailableBooks(string availableFor);
        Task<List<Books>> GetUnavailableBooks();
        Task<bool> UpdateBook(Books book);
        Task<int> GetFee(int userId, int bookId, DateTime rentingBook, DateTime returningBook);
    }
}
