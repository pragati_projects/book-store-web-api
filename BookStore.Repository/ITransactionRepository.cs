﻿using BookStore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Repository
{
    public interface ITransactionRepository
    {
        Task<bool> AddTransaction(Transaction transaction);
        Task<List<Transaction>> GetTransactionDetails();
    }
}
