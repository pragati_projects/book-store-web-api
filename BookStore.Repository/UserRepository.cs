﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BookStore.Entities;
using BookStore.Repository;
using BookStoreCustomExceptions;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly BookStoreDbContext _bsDbContext;

        public UserRepository(BookStoreDbContext bookStoreDbContext)
        {
            _bsDbContext = bookStoreDbContext;
        }

        public async Task<bool> AddUser(Users user)
        {
            int EntryAdded = 0;
            try
            {
                _bsDbContext.Add(user);
                EntryAdded = await _bsDbContext.SaveChangesAsync();
                if (EntryAdded == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                throw new SQlException("couldnt connect", ex);
            }
        }

        public async Task<bool> DeleteUser(int id)
        {
            try
            {
                Users user = await _bsDbContext.User.FirstOrDefaultAsync(u => u.UserId == id);
                if (user == null)
                {
                    throw new NoDataFound("No user present! Please add the user");
                }
                else
                {
                    user.IsDeleted = true;
                    int EntryAdded = 0;
                    EntryAdded = await _bsDbContext.SaveChangesAsync();
                    if (EntryAdded == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch(NoDataFound ex)
            {
                throw new NoDataFound(ex.Message);
            }
            catch(Exception ex)
            {
                throw new SQlException("couldnt connect", ex);
            }
        }

        public async Task<List<Users>> GetUser()
        {
            try
            {
                List<Users> user = await _bsDbContext.User.ToListAsync();
                return user;
            }
            catch(Exception ex)
            {
                throw new SQlException("Couldnt connect", ex);
            }
        }

        public async Task<bool> UpdateUser(Users user)
        {
            try
            {
                Users userTemp = await _bsDbContext.User.FirstOrDefaultAsync(u => u.UserId == user.UserId);
                if (userTemp != null)
                {
                    userTemp.UserName = user.UserName;
                    userTemp.Email = user.Email;

                    int EntryAdded = 0;
                    EntryAdded = await _bsDbContext.SaveChangesAsync();
                    if (EntryAdded == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    throw new NoDataFound("No user present! Please add user details");
                }
            }
            catch(NoDataFound ex)
            {
                throw new NoDataFound(ex.Message);
            }
        }
    }
}
